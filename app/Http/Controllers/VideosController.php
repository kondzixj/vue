<?php

namespace App\Http\Controllers;

use Request;
use App\Video;
use App\Http\Requests\CreateVideoRequest;
use Auth;
use Session;
class VideosController extends Controller
{
    public function __construct(){
        $this->middleware('auth', ['only'=>'create']);
    }

    //pobieramy liste filmów z bazy
    public function index(){
        $videos = Video::latest()->get();
        return view('videos.index')->with('videos', $videos);   
    }
    
    //pobieranie jednego filmu z bazy
    public function show($id){
        $video = Video::findOrFail($id);
        return view('videos.show')->with('video', $video);
    }

    //wyswietal formularz dodawania filmu
    public function create(){
        return view('videos.create');
    }

    //Zapisywanie filmow do bazy
    public function store(CreateVideoRequest $request){
        $video = new Video($request->all());
        Auth::user()->videos()->save($video);
        Session::flash('video_created', 'Twój film został zapisany');
        return redirect('videos');
    }

    //Edytowanie filmów
    public function edit($id){
        $video = Video::findOrFail($id);
        return view('videos.edit')->with('video', $video);
    }

    //Aktualizacja filmów
    public function update($id, CreateVideoRequest $request){
        $video = Video::findOrFail($id);
        $video->update($request->all());
        return redirect('videos');
    }
}